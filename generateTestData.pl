#!/usr/bin/perl
# generateDat.pl
# Generates test data file for jaat.pl

use strict;
use Switch;

# Data (out) file and the config file location.
my $data_file="./jaat_data.csv";
my $config_file = "./config.ini";


#-----------------------------------------------#
# Main function                                 
#-----------------------------------------------#

# Load the config and set the dicom output dir.
my %config = loadConfig();
my $dicom_dir=$config{FINDSCU_OUT_DIR};

# Pull dicom from the test server.
pullDcm();

# Get a list of files.
my @files = getFiles();
my $queryType;

# Generate the data file. 
print "Generating $data_file...\n";
for my $i (0..$#files){
   $queryType = getQueryType();  # Determine what type of data to build. 
   switch ($queryType){
      case 1 { queryTypeOne($files[$i],$i);   }
      case 2 { queryTypeTwo($files[$i],$i);   }
      case 3 { queryTypeThree($files[$i],$i); }   
   }
}

# Remove the directory containing the dicom.
#print "Removing $dicom_dir..."
#`rm -r $dicom_dir`

print "Done.\n";
exit;

#-----------------------------------------------------#
# Subroutine to pull dicom from the test server.  
# Warning: This pulls all sessions.
#-----------------------------------------------------#
sub pullDcm {
  unless(-d $dicom_dir){
     print "Creating out dir $dicom_dir\n";
     mkdir $dicom_dir or die "Unable to create dicom dir."
  }  
  print "Pulling dicom from test server\n";
  my $cmd = "findscu -q -X -S -k PatientName=\"\" -k StudyDate=\"\" -k AccessionNumber=\"\" -k Modality=\"\" -k PatientBirthDate=\"\" -k PatientID=\"\" -k StudyInstanceUID=\"*\" --aetitle NRG-CLINICAL1 --call $config{EMAGEON_TEST_AE} $config{EMAGEON_TEST_HOST} $config{EMAGEON_TEST_PORT}";
  `cd $dicom_dir && $cmd; cd ..`;
   my $num = `ls -1 $dicom_dir | wc -l`;
   chomp $num;
   print "$num dicom files retrieved.\n";
}

#-----------------------------------------------------#
# Subroutine to build a row for query type 1.
#-----------------------------------------------------#
sub queryTypeOne {
#   print "Building Test data for query type 1\n";
   my $accNum = extractAttribute("$_[0]", "AccessionNumber");
   my $row="1,JenTest,testSubj_$_[1],testSession_$_[1],$accNum\n";
   appendRow($row);
}

#-----------------------------------------------------#
# Subroutine to build a row for query type 2.
#-----------------------------------------------------#
sub queryTypeTwo {
#   print "Building Test data for query type 2\n";
   my $accNum = extractAttribute("$_[0]", "AccessionNumber");
   my $pid = extractAttribute("$_[0]", "PatientID");
   my $row="2,JenTest,testSubj_$_[1],testSession_$_[1],$accNum/$pid\n";
   appendRow($row);
}

#-----------------------------------------------------#
# Subroutine to build a row for query type 3.
#-----------------------------------------------------#
sub queryTypeThree {
#   print "Building Test data for query type 3\n";
   my @pName = split('\^',extractAttribute("$_[0]", "PatientName"),2);
   my $studyDt = extractAttribute("$_[0]", "StudyDate");
   my $bday = extractAttribute("$_[0]", "PatientBirthDate");
   my $mod = extractAttribute("$_[0]", "Modality");
   my $row="3,JenTest,testSubj_$_[1],testSession_$_[1],$pName[0]/$pName[1]/$studyDt/$bday/$mod\n";
   appendRow($row);
}

#-----------------------------------------------------#
# Subroutine to append a row to the data file.
#-----------------------------------------------------#
sub appendRow {
  open FILE, ">> $data_file" or die "Unable to open out file $data_file";
  print FILE "$_[0]";
  close FILE;
}

#-----------------------------------------------------#
# Subroutine to extract an attribute from a dicom file
#-----------------------------------------------------#
sub extractAttribute {                                                                                                                                                                          
   # Build the command that extracts the uid from dcmdump.
#   print " Extracting $_[1] from $_[0]\n";
   my $cmd = "dcmdump --search $_[1] $_[0] | awk -F [][] '{print \$2}'";                        
   my $r = `$cmd`;
   chomp $r;        
   return $r;       
}      

#-----------------------------------------------------#
# Subroutine randomly picks a number between 1 and 3
#-----------------------------------------------------#
sub getQueryType {
   my $min = 1;
   my $max = 3;
   return (int(rand($max)) + $min);
}

#-----------------------------------------------------#
# Subroutine gets a list of files from the dicom 
# directory.
#-----------------------------------------------------#
sub getFiles {
   my @f;
   foreach(<$dicom_dir/*>){
      next unless m/\.dcm$/; #ignore non dicom files.
      push (@f, $_);
   }
   return @f;
}

#-----------------------------------------------------#
# Subroutine to load the configuration file           
#-----------------------------------------------------#
sub loadConfig {                                                                                                                                                                                
   # Check if the config file exists.
   unless(-e $config_file){
      print "|ERROR|>> Config file \"$config_file\" does not exist!\n";
      print "Aborting ... \n";
   }   
        
   # Try to open the file 
   open(FILE, $config_file) or die "|ERROR|>> Unable to open the config file.\n";
        
   # Populate hash with config contents
   my %info;
   for(<FILE>){
      next if /^#/;
      chomp;
      my($name, $val) = split '=', $_, 2;
      next unless $val;
      $info{$name} = $val;
   }   
        
   close FILE;  
   # Make sure the user has actually looked at the configuration file. 
   print "Please verify configuration file contents. ($config_file)\n" and exit unless $info{EVERYTHING_IS_OK} eq "1";
   return %info;
}  
