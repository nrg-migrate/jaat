#!/usr/bin/perl
###########################################################
###########################################################
##            _____  ______  ______  ______              ##
##           /\___ \/\  _  \/\  _  \/\__  _\             ##
##           \/__/\ \ \ \L\ \ \ \L\ \/_/\ \/             ##
##              _\ \ \ \  __ \ \  __ \ \ \ \             ##
##             /\ \_\ \ \ \/\ \ \ \/\ \ \ \ \            ##
##             \ \____/\ \_\ \_\ \_\ \_\ \ \_\           ##
##              \/___/  \/_/\/_/\/_/\/_/  \/_/           ##
##                                                       ##
##            Joint Archive and Anonymization Tool       ##
##                                                       ##
###########################################################
###########################################################
#  
# Tool to pull dicom files from a PACS server and send 
# them to a CTP server without knowing the study instance
# uid. The tool does a findscu operation to obtain the
# study instance uid and then a movescu to move the 
# dicom from the PACS to the CTP.  Similar to qarchive.pl 
# except jaat.pl does not depend on user input. All options 
# are set in the config file or by cmd line. Some logic 
# was recycled from qarchive. This script also will also
# generate a remap file to tell the CTP how to anonymize 
# the dicom files.

use strict;
use Switch;

# Location of the configuration file.
my $config_file="./config.ini";

#------------------------------------------------------#
# Begin Main Function                                  #
#------------------------------------------------------#
print "jaat.pl v1.0\n";

# Load the configuration.
my %config = loadConfig();
print "|> Successfully loaded configuration.\n";

# Parse the comand line.
parseArgs();
print "|> Successfully parsed cmd line args.\n-------------------------------------\n";

# Load the data file
my @data = loadData();

#Delete the remap file if configured to.
if($config{DELETE_REMAP} == 1){
   printDbg("Deleting Remap File: $config{REMAP_FILE}\n");
   unlink $config{REMAP_FILE};
}

# Get the archive information. 
my ($ae, $host, $port) = selectArchive();
printDbg("Using Archive: $ae $host:$port\n");

#For each row in the data file.
my ($fFailed, $mFailed, $numMoved, $msg, @line, $uids);
foreach my $row (@data){
   ($fFailed, $mFailed, $numMoved) = (0,0,0);
   $msg = "";
   
   # split up the csv.
   @line = split(',', $row);
 
   # Column 5 indicates the row has already been processed.
   next if $line[5];

   printDbg("Processing row: \"@line\"\n");
   
   # Strip whitespace
   foreach (@line) {$_ =~ s/\s//g;} 
 
   # Make sure we are alowed to contact the PACS at this time
   checkTime();

   # Look up the session on the PACS. dicom files stored in $config{FINDSCU_OUT}
   ($fFailed, $msg, $uids) = lookupSessions(@line);
   
   if($fFailed){
      $msg = "Find Operation Failed" and printDbg($msg."\n");
   }elsif(!@{$uids}){
      $msg = "Couldn't find anything matching this session." and printDbg($msg."\n");
   }elsif(scalar(@{$uids}) > $config{MAX_SESSION}){
      $msg = "Found too many uids ".$msg.scalar(@{$uids})." found";
      printDbg("Found too many Uids.  Cannot process more than $config{MAX_SESSION}. Found ".scalar(@{$uids})."\n");
   }else{
      $msg = "Found ".scalar(@{$uids})." uids".$msg;
      printDbg("Number of Uids found: ".scalar(@{$uids})."\n");
      # Request Move from PACS to CTP
      $mFailed = moveSessions($uids, \@line);
      
      $numMoved = scalar(@{$uids})-$mFailed;   # number moved is total uids - mFailed.
      $msg = $msg.", Moved $numMoved studies";
      printDbg("Moved $numMoved studies\n");
   }

   # Add a timestamp and status msg and update the data file.
   $row=$row.", Processed On: ".getTimeStamp().", ".$msg;
   updateDataFile();

   printDbg("Done processing row.\n");
   # Only wait if there is something else to process
   unless ($row eq $data[-1]){
      # If we are waiting in between finds (i.e. find operation failed) we use FIND_SLEEP
      if ($fFailed){
         printDbg("Waiting $config{FIND_SLEEP} minutes...\n") and sleep 60*int($config{FIND_SLEEP})
      }else{ 
         printDbg("Waiting $config{SESSION_SLEEP} minutes...\n") and sleep 60*int($config{SESSION_SLEEP});
}}}

printDbg("No more data to process.\n") and printDbg("DONE!\n");
exit;

#------------------------------------------------------#
# Subroutine to lookup additional info about a session #
#------------------------------------------------------#
sub lookupSessions(){
   prepareOutDir();                               # Prepare the output directory for writing
   my $retMsg = "";                               # Message to return to calling function
   my @params = split('/', $_[4]);                # Search parameters stored in column 4 of the csv. 

   my ($st, $uids) = attemptFind($_[0], @params); # Attempt to find the study 

   # If Type3 Refined search is enabled. Try to find the session with patient first initial 
   # instead of first name and set patient birthday to '*'
   if($config{TYPE_3_REFINED_SEARCH} && (($_[0] == 3) && !@{$uids})){
      printDbg("Couldn't find anything matching this session.\n");
      printDbg("Refining search...\n");
      $params[1] = substr($params[1],0,1);         # Set Firstname to First inital
      ($st, $uids) = attemptFind($_[0], @params);  # Attempt to find the study again
      $retMsg = " using the refined search. (First Inital, Last Name, Birthday)" if (!$st && @{$uids}); 

      # Refine further if we fail agian.
      if(!@{$uids}){
         printDbg("Couldn't find anything matching this session.\n");
         printDbg("Refining search further\n");
         $params[3]='*';                             # Set birthday to wildcard.     
         ($st, $uids) = attemptFind($_[0], @params); # Attempt to find the study again 
         $retMsg = " using the refined search. (First Inital, Last Name)" if (!$st && @{$uids}); 
   }}
   return ($st,$retMsg,$uids);
}

#-------------------------------------------------------------------------#
#  Subroutine will try too find the session base on $_                    #
#-------------------------------------------------------------------------#
sub attemptFind(){
   my $query = getQueryString(@_);  # Build the query with parameters in @_
   my ($status,$time) = (0,0);      # Initalize the status and time to 0

   # Attempt to find the session $config{FIND_RETRY} times.
   for my $j (1..$config{FIND_RETRY}){
      ($status, $time) = (0,0); 
      printDbg("Executing Find...\n");
      printDbg("Attempt $j/$config{FIND_RETRY}\n"); 

      $time = time();  # Save the time so we can calculate the time it took.

      # Execute query to retrieve study instance uids. Return if we fail.
      $status = executeFind($config{FINDSCU_OUT_DIR},$query,$config{SRC_AE},$ae,$host,$port); 
      printDbg("Find status = $status\n");
      
      # Output the time it took..
      printDbg(sprintf("Find took %d seconds.\n", time() - $time));
      last unless $status;

      # Find op failed if we got here.
      printDbg("Find operation failed!\n");
      printDbg("Trying again in $config{FIND_SLEEP} minutes.\n");
      sleep 60*int($config{FIND_SLEEP});
   }
   return ($status, getStudyUids());
}

#-----------------------------------------------------#
# Subroutine to get the query string to execute       #
#-----------------------------------------------------#
sub getQueryString {
   switch($_[0]){  # Switch on Query Type (1, 2, 3)
      case 1  {
         printDbg("Building query by accession number\n");
         printDbg("Using Accession Number:\"$_[1]\"\n");
         return "-k PatientName=\"\" -k AccessionNumber=\"$_[1]\" -k PatientID=\"\" -k StudyInstanceUID=\"\""; 
      }
      case 2  { 
         printDbg("Building query by accession number and pid.\n");
         printDbg("Using Accession Number: \"$_[1]\"\n");
         printDbg("Using Patient ID: \"$_[2]\"\n");
         return "-k PatientName=\"\" -k AccessionNumber=\"$_[1]\" -k PatientID=\"$_[2]\" -k StudyInstanceUID=\"\""; 
      }
      case 3  { 
         printDbg("Building query by patient name, study date, patient birthdate, and modality.\n");
         printDbg("Using Patient Name: \"$_[2] $_[1]\"\n");
         printDbg("Using Study Date: \"$_[3]\"\n");
         printDbg("Using Patient Birthdate: \"$_[4]\"\n");
         printDbg("Using Modality: \"$_[5]\"\n");
         return "-k PatientName=\"$_[1]$config{PATIENT_NAME_DELEM}$_[2]*\" -k StudyDate=\"$_[3]\" -k PatientBirthDate=\"$_[4]\" -k Modality=\"$_[5]\" -k PatientID=\"\" -k StudyInstanceUID=\"\"";
}}}

#
#------------------------------------------------------#
# Subroutine to request a move from the pacs           #
#------------------------------------------------------#
sub moveSessions {
   my @params = @{$_[1]};                      # Array of information collected from the CSV file.
   my @uids = @{$_[0]};                        # Uids Extracted from dicom sent back from the find operation. 
   my ($st, $failed, $mTime, $mElapsed) = 0;   # Status, Failed, Time started, Time elapsed. 
   my ($mSTime, $mETime) = "";                 # Start / End Timestamps for Report file.
   my $sessionLabel = $line[3];                # The Session label to send to CTP.
   
   # For each uid returned, request a move from the pacs
   for my $i (0..$#uids){
      # If we have more than 1 uids append the index to the session label.
      $sessionLabel = sprintf("%s_part%d",$params[3],$i+1) if ($#uids > 0);

      # Wait 60 seconds in between moving. If there are more than 1 UIDS to move.
      printDbg("Waiting $config{MOVE_SLEEP} minutes before processing next UID.\n") and sleep 60*int($config{MOVE_SLEEP}) if($i>0);

      # Generate the remap file for this session.
      $st = generateRemapFile($params[1],$params[2], $sessionLabel, $uids[$i]);
      if ($st){
         # next if we couldn't generate remap file.
         printErr("Failed to generate remap file for uid: $uids[$i]. Skipping ... \n");
         $failed++;
         next;
      }
     
      # Keep retrying until we succeed or exceed the under of retrys. 
      for my $j (1..$config{MOVE_RETRY}){
         printDbg("Executing move for session with UID $uids[$i] ...\n");
         printDbg("Attempt $j/$config{MOVE_RETRY}\n");

        # Save start time so we can calculate how long it took. 
         $mTime = time();
         $mSTime = getTimeStamp("timeOnly");

         # Execute the move ... $st > 0 if we failed. 
         $st = executeMove($uids[$i], $config{SRC_AE}, $ae, $host, $port, $config{TARGET_AE});

         # Output the time it took..
         $mElapsed = time() - $mTime;
         $mETime = getTimeStamp("timeOnly");
         printDbg(sprintf("Move took %d seconds.\n", $mElapsed));

         # Keep retrying until we succeed.
         printDbg("Move status = $st\n");
         generateReport($params[1], $params[2], $sessionLabel, $mSTime, $mETime, $mElapsed ) and last unless $st;         

         # Move op failed if we got here.
         printDbg("Move operation failed for UID: $uids[$i]\n");
         printDbg("Trying again in $config{MOVE_SLEEP} minutes.\n");
         sleep 60*int($config{MOVE_SLEEP});
      }
      # If we never succeeded.
      $failed++ and printErr("Unable to preform C-Move for UID: $uids[$i]. Giving up ...\n") if $st;
   }
   return $failed; # Return the number of failed attempts.  
}

#-----------------------------------------------------#
# Subroutine to append information to the report file #
#-----------------------------------------------------#
sub generateReport {
   unless(-d $config{REPORTS_DIR}){ # If the directory does not exist create it. 
      printDbg("Creating reports directory \"$config{REPORTS_DIR}\"...\n");
      my $st = mkdir $config{REPORTS_DIR}, 0775;
      printErr("Unable to create reports directory $config{REPORTS_DIR}!\n") and return unless $st;
   }

   # Get params and timestamp
   my ($proj, $subj, $sess, $startTime, $endTime, $elapsedTime) = @_;
   my $date = getTimeStamp("dateOnly");
 
   # Open todays report file.
   open (FILE, ">> $config{REPORTS_DIR}/jaat_report_$date.csv") or printErr("Unable to open report file!\n") and return;
   
   # Reformat the date for the report file. (replace _ with / for readability)
   $date =~ s/_/\//g;

   # Append report information.
   printDbg("Appending to Report file: $date @_\n");
   print FILE "$date, $proj, $subj, $sess, $startTime, $endTime, $elapsedTime\n";
   close FILE;
}

#------------------------------------------------------#
# Subroutine to generate the remap file.               #
#------------------------------------------------------#
sub generateRemapFile {
   my ($project, $subject, $session, $studyId) = @_; 
   # Try to open the file or return error.
   open (FILE, ">> $config{REMAP_FILE}") or return 1; 
   print FILE "stddesc/$studyId=$project\nptname/$studyId=$subject\nptid/$studyId=$session\n";
   close FILE;

   # Sleep to make sure the CTP can get the changes.
   sleep 5 and return 0;
}

#------------------------------------------------------#
# Subroutine to get the Patient Id for this session.   #
#------------------------------------------------------#
sub getPatientId {
   # Try to open the output directory.
   opendir DIR, $config{FINDSCU_OUT_DIR} or die "Unable to open direcotry: $config{FINDSCU_OUT_DIR}\n";
   for(readdir(DIR)){
      my $pid = extractAttribute("$config{FINDSCU_OUT_DIR}/$_","PatientID") if m/\.dcm$/;
      return $pid if $pid;
   }
   closedir DIR and return 0;
}

#------------------------------------------------------#
# Subroutine to get a list of study uids from all      #
# dicom files within a directory.                      #
#------------------------------------------------------#
sub getStudyUids {
   # Try to open the output directory.
   opendir DIR, $config{FINDSCU_OUT_DIR} or printErr("Unable to open direcotry: $config{FINDSCU_OUT_DIR}\n") and exit;
   
   printDbg("Getting Study Uids ... \n");
   my (%h, @uids, $uid);
   for(readdir(DIR)){ 
      next unless m/\.dcm$/; # Skip non dcm files.
      # Store the uid as a key in the hash so there are no duplicates.
      $uid =  extractAttribute("$config{FINDSCU_OUT_DIR}/$_", "StudyInstanceUID");
      $h{$uid} = 1; # bogus value.
   }
   closedir DIR;
   @uids = keys %h; # Return the keys in the hash
   return \@uids;
}

#------------------------------------------------------#
# Subroutine to extract an attribute from a dicom file #
#------------------------------------------------------#
sub extractAttribute {
   my $cmd = "dcmdump --search $_[1] $_[0] | awk -F [][] '{print \$2}'";
   my $r = `$cmd`;
   
   printDbg("Extracting $_[1] from $_[0]\n");
   chomp $r and return $r;
}

#------------------------------------------------------#
# Subroutine to execute the C-Move command.            #
#------------------------------------------------------#
sub executeMove {
   my ($stdyUID, $srcAE, $dstAE, $dstHost, $dstPort, $targetAE) = @_; 
   my $cmd = "movescu -S -k QueryRetrieveLevel=\"STUDY\" -k StudyInstanceUID=\"$stdyUID\" --aetitle $srcAE --move $targetAE --call $dstAE $dstHost $dstPort";

   print "> $cmd\n" and `$cmd`;
   return $?;
}

#-----------------------------------------------------#
# Subroutine to execute the query                     #
#-----------------------------------------------------#
sub executeFind{
   my ($outDir, $queryStr, $srcAE, $dstAE, $dstHost, $dstPort) = @_;
   my $dbg = $config{PRINT_DEBUG} ? "" : "-q"; # Supress findscu output if debug is off.
   my $cmd = "findscu $dbg -X -S -k QueryRetrieveLevel=\"STUDY\" $queryStr --aetitle $srcAE --call $dstAE $dstHost $dstPort";

   printDbg("Executing Find ... \n");
   print "> $cmd\n" and `cd $outDir; $cmd`;
   return $?;
}

#-----------------------------------------------------#
# Subroutine to create output directory               #
#-----------------------------------------------------#
sub prepareOutDir {
   unless(-d $config{FINDSCU_OUT_DIR}){ # If the directory does not exist create it. 
      printDbg("Creating output directory \"$config{FINDSCU_OUT_DIR}\"...\n");
      my $st = mkdir $config{FINDSCU_OUT_DIR}, 0775;
      printErr("Unable to create output directory $config{FINDSCU_OUT_DIR}\n") and exit unless $st;
   }else{ # Otherwise clean the directory.
      printDbg("Scrubbing output directory...\n");
      opendir DIR, $config{FINDSCU_OUT_DIR} or printErr("Unable to open the output directory \"$config{FINDSCU_OUT_DIR}\"\n") and exit;
      for(readdir(DIR)){
         next if m/^\.$|^\.\.$/; # Don't try to delete these dirs
         printDbg("Deleting file $_.\n"); 
         unlink("$config{FINDSCU_OUT_DIR}/$_") or warn "Unable to unlink file: $config{FINDSCU_OUT_DIR}/$_\n";
      }
      closedir DIR;
}}

#-----------------------------------------------------#
# Subroutine to create select the correct archive.    #
#-----------------------------------------------------#
sub selectArchive { 
   # Return test archive if test mode is on.
   return ($config{PACS_TEST_AE},$config{PACS_TEST_HOST}, $config{PACS_TEST_PORT}) if $config{USE_TEST_ARC};

   # Otherwise return the production archive.
   return ($config{PACS_AE},$config{PACS_HOST}, $config{PACS_PORT});
}

#------------------------------------------------------#
# Subroutine that checks to see if we are allowed to   #
# be running based on the current time.                #
#------------------------------------------------------#
sub checkTime {
   # Get the current time, the start time, and the end time. 
   my $time  = ((localtime)[2] * 100) + (localtime)[1];
   my $sTime = $config{START_TIME};
   my $eTime = $config{END_TIME};
   
   printDbg("Time:\t\t$time\n|DEBUG|>> Start Time:\t$sTime\n|DEBUG|>> End Time:\t$eTime\n");

   # Determine if the current time is between the start time and end time.
   if((($sTime < $eTime) and ($time < $eTime) and ($time >= $sTime)) or 
      (($eTime < $sTime) and (($time < $eTime) or ($time >= $sTime))))
   {
      printDbg("Time OK.\n") and return;
   }
   # Exit if we make it here. 
   print "|> I'm sorry Dave, I cannot continue execution due to time restrictions.\n" and exit;
}

#-----------------------------------------------------#
# Subroutine that gets the current time stamp         #
#-----------------------------------------------------#
sub getTimeStamp {
   my @m = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
   my @tArr = localtime; 
   my $year = $tArr[5] + 1900;
   my $dateStr = "";

   switch($_[0]){
      # Build a date only timestamp (Jan_2_2013)
      case "dateOnly" { $dateStr = "$m[$tArr[4]]_$tArr[3]_$year"; }
      # Build a time only timestamp (14:32:44)
      case "timeOnly" { $dateStr = sprintf("%02d:%02d:%02d", $tArr[2], $tArr[1], $tArr[0]); }
      # Build a time and date timestamp (Jan 2 14:32:44 2013)
      else { $dateStr = sprintf("%s %d %02d:%02d:%02d %d", $m[$tArr[4]], $tArr[3], $tArr[2], $tArr[1], $tArr[0], $year); }
   }
   # Return the formatted date string.
   return $dateStr;
}

#-----------------------------------------------------#
# Subroutine to write @data array back to the data    #
# file                                                #
#-----------------------------------------------------#
sub updateDataFile(){
   #Try to open the file.
   open (FILE, ">$config{DATA_FILE}") or printErr("Unable to open the data file.\n") and exit;
   
   printDbg("Writing data file ...\n");
   for(@data){ print FILE "$_\n"; } 
   close FILE;
}

#------------------------------------------------------#
# Subroutine to load the data file                     #
#------------------------------------------------------#
sub loadData {
   # Print error if no data file has been specified.
   printErr("No data file specified! Aborting ... \n") and printHelp() if ($config{DATA_FILE} eq "");
   
   # Print error and exit if the data file does not exist.
   printErr("Data File \"$config{DATA_FILE}\" Does Not Exist! Aborting ... \n") and exit unless (-e $config{DATA_FILE});

   # Try to open the file. 
   open(FILE, $config{DATA_FILE}) or printErr("Unable to open data file.\n") and exit;
   printDbg("Loading data from data file \"$config{DATA_FILE}\"\n");
   
   my @info;
   for(<FILE>){ 
      chomp;
      push(@info, $_) if $_; 
   }
   close FILE and return @info;
}

#-----------------------------------------------------#
# Subroutine to load the configuration file           #
#-----------------------------------------------------#
sub loadConfig {   
   # Print error and exit if the config file does not exist.
   printErr("Config file \"$config_file\" Does Not Exist! Aborting ... \n") and exit unless(-e $config_file);
  
   # Try to open the file 
   open(FILE, $config_file) or printErr("Unable to open the config file.\n") and exit;

   my %info;
   for(<FILE>){
      next if /^#/; # Skip comment.
      chomp;
      my($name, $val) = split '=', $_, 2;
      $info{$name} = $val if $val; 
   }
   close FILE;  
   
   # Make sure the user has actually looked at the configuration file. 
   printErr("Please verify configuration file contents. ($config_file)\n") and exit unless($info{EVERYTHING_IS_OK} eq "1");
   return %info;
}

#----------------------------------------------------#
# Subroutine to parse the command line.              #
#----------------------------------------------------#
sub parseArgs {
   foreach my $arg (0 .. $#ARGV){
     switch($ARGV[$arg]){
        case /-h|--help/             { printHelp ();                      }
        case /-d|--debug/            { $config{PRINT_DEBUG}=1;            }
        case /-f|--file/             { $config{DATA_FILE}=$ARGV[$arg+1];  }
        case /-t|--test/             { $config{USE_TEST_ARC}=1;           }
        case /-et|--end-time/        { $config{END_TIME}=$ARGV[$arg+1];   }
        case /-st|--start-time/      { $config{START_TIME}=$ARGV[$arg+1]; }
        case /-or|--overwrite-remap/ { $config{DELETE_REMAP}=1;           }
}}}

#---------------------------------------------------#
# Subroutine to print the help dialog.              #
#---------------------------------------------------#
sub printHelp {
   print "Usage: jaat.pl [OPTION]...\n",
         "\t-f,  --file <data_file>    Data file to load.\n",
         "\t-st, --start-time          Start time override. HHMM format.\n",
         "\t-et, --end-time            End time override. HHMM format.\n",
         "\t-or, --overwrite-remap     Deletes the remap file when the script starts.\n",
         "\t-h,  --help                Print this menu.\n",
         "\t-t,  --test                Use test Server.\n",
         "\t-d,  --debug               Enable debug prints.\n";
   exit;
}

#---------------------------------------------------#
# Subroutine to print debug messages.               #
#---------------------------------------------------#
sub printDbg { print "|DEBUG|>> $_[0]" if $config{PRINT_DEBUG}; }

#--------------------------------------------------#
# Subroutine to print error messages.              #
#--------------------------------------------------#
sub printErr { print "|ERROR|>> $_[0]"; }
